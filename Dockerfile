FROM registry.gitlab.com/legend_kitchen/docker:latest

ENV \
  APP_HOME="/app"

WORKDIR $APP_HOME

# Установка гемов
COPY Gemfile* ./
RUN bundle install
# RUN bundle install --without development test

# Копирование исходников
COPY . .

# Установка ассетов
RUN yarn install --check-files && \
    RAILS_ENV=production bundle exec rake assets:precompile

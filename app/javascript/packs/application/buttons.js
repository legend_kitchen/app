$(document).on('click', '.button', function(event) {
  href = event.target.getAttribute('href');
  if(href) {
    event.preventDefault();
    window.location = href;
  }
});

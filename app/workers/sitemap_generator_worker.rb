# frozen_string_literal: true

require 'sidekiq-scheduler'

class SitemapGeneratorWorker
  include Sidekiq::Worker

  def perform
    SitemapGeneratorService.call
  end
end

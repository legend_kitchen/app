window.init_tinymce = (images_upload_url) ->
  tinyMCE.init
    selector: 'textarea',
    height: 500,
    plugins: 'image | link',
    images_upload_url: images_upload_url

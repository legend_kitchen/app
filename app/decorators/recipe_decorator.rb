# frozen_string_literal: true

class RecipeDecorator < ApplicationDecorator
  delegate_all

  decorates_association :kitchen

  def formatted_ingredients
    ingredients.map do |ingredient|
      "#{ingredient.product.name}: #{ingredient.amount}"
    end.join("\n")
  end
end

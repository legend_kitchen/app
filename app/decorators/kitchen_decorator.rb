# frozen_string_literal: true

class KitchenDecorator < ApplicationDecorator
  delegate_all

  def formatted_name
    "#{name} #{I18n.t('activerecord.models.kitchen').downcase}"
  end
end

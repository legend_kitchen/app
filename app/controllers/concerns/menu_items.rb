# frozen_string_literal: true

module MenuItems
  DASHBOARD = :dashboard
  RECIPES = :recipes
  KITCHENS = :kitchens
end

# frozen_string_literal: true

class ApplicationController < ActionController::Base
  BREADCRUMB = Struct.new(:url, :text)

  protected

  def add_breadcrumb(url, text)
    @breadcrumbs ||= []
    @breadcrumbs << BREADCRUMB.new(url, text)
    @breadcrumbs.last
  end
end

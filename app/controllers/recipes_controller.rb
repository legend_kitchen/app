# frozen_string_literal: true

class RecipesController < ApplicationController
  def show
    @recipe = Recipe.published.includes(:kitchen, :category, :ingredients).friendly.find(params[:id]).decorate
  rescue ActiveRecord::RecordNotFound
    redirect_to(action: :index)
  end

  private

  def index_params
    params.permit(:page)
  end
end

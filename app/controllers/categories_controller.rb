# frozen_string_literal: true

class CategoriesController < ApplicationController
  before_action :set_active_menu_item

  def index
    @categories = Category.all.page(index_params[:page])
  end

  def show
    category = Category.friendly.find(params[:id])
    @recipes = Recipe.published
                     .includes(:kitchen, :category)
                     .where(category: category)
                     .page(index_params[:page])
                     .decorate
    add_breadcrumb(categories_path, I18n.t('model_names.categories'))
    add_breadcrumb(category_path(category.slug), category.name)
  end

  private

  def index_params
    params.permit(:page)
  end

  def set_active_menu_item
    @active_menu_item = MenuItems::RECIPES
  end
end

# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action :set_active_menu_item

  def index
    @categories = Category.all.limit(8)
    @kitchens = Kitchen.all.limit(8)
    @recipes = Recipe.published.includes(:kitchen, :category).limit(8).decorate
  end

  private

  def set_active_menu_item
    @active_menu_item = MenuItems::DASHBOARD
  end
end

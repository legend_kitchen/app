# frozen_string_literal: true

class KitchensController < ApplicationController
  before_action :set_active_menu_item

  def index
    @kitchens = Kitchen.all.page(index_params[:page])
  end

  def show
    kitchen = Kitchen.friendly.find(params[:id]).decorate
    @recipes = Recipe.published
                     .includes(:kitchen, :category)
                     .where(kitchen: kitchen)
                     .page(index_params[:page])
                     .decorate
    add_breadcrumb(kitchens_path, I18n.t('model_names.kitchens'))
    add_breadcrumb(kitchen_path(kitchen.slug), kitchen.formatted_name)
  end

  private

  def index_params
    params.permit(:page)
  end

  def set_active_menu_item
    @active_menu_item = MenuItems::KITCHENS
  end
end

# frozen_string_literal: true

ActiveAdmin.register Kitchen do
  menu priority: 1, label: proc { I18n.t('active_admin.kitchen') }

  permit_params(
    :name, :cover
  )

  filter :name

  index do
    column :id
    column :name
    column :recipes_count do |kitchen|
      kitchen.recipes.count
    end

    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :cover do |kitchen|
        image_tag kitchen.cover.card.url
      end
      row :recipes_count do |kitchen|
        kitchen.recipes.count
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors

    f.inputs do
      f.input :name
      f.input :cover, as: :file, hint: image_tag(f.object.cover.card.url)
    end

    f.actions
  end
end

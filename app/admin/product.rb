# frozen_string_literal: true

ActiveAdmin.register Product do
  menu priority: 1, label: proc { I18n.t('active_admin.product') }

  permit_params(
    :name
  )

  filter :name

  index do
    column :id
    column :name
    column :recipes_count do |product|
      product.recipes.count
    end

    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :recipes_count do |product|
        product.recipes.count
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors

    f.inputs do
      f.input :name
    end

    f.actions
  end
end

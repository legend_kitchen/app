# frozen_string_literal: true

ActiveAdmin.register Recipe do
  menu priority: 1, label: proc { I18n.t('active_admin.recipe') }

  decorate_with RecipeDecorator

  # TODO: solve this problem by another way
  skip_before_action :verify_authenticity_token, only: :upload_image

  collection_action :upload_image, method: :post do
    uploader = RecipeBodyImageUploader.new
    uploader.store!(params['file'])
    render(json: { location: uploader.url }.to_json, status: :ok)
  end

  permit_params(
    :name, :cover, :status, :body, :category_id, :kitchen_id, ingredients_attributes: %i[id product_id amount _destroy]
  )

  filter :name
  filter :status
  filter :created_at

  index do
    column :id
    column :name
    column :category do |recipe|
      recipe.category.name
    end
    column :kitchen do |recipe|
      recipe.kitchen&.name
    end
    column :status
    column :created_at

    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :cover do |recipe|
        image_tag recipe.cover.card.url
      end
      row :category do |recipe|
        recipe.category.name
      end
      row :kitchen do |recipe|
        recipe.kitchen&.name
      end
      row :ingredients do |recipe|
        recipe.formatted_ingredients.gsub("\n", '<br/>').html_safe
      end
      row :status
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors

    f.inputs do
      f.input :name
      f.input :cover, as: :file, hint: image_tag(f.object.cover.card.url)
      f.input :category, as: :select, collection: Category.collection_for_select(:name)
      f.input :kitchen, as: :select, collection: Kitchen.collection_for_select(:name)
      f.has_many :ingredients, allow_destroy: true, new_record: true do |ingredient_f|
        ingredient_f.input :product, as: :select, collection: Product.collection_for_select(:name)
        ingredient_f.input :amount
      end
      f.input :status, include_blank: false
      f.input :body, input_html: { class: 'tinymce' }
    end

    f.actions

    script { raw 'init_tinymce("/admin/recipes/upload_image");' }
  end
end

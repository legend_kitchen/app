# frozen_string_literal: true

class Kitchen < ApplicationRecord
  include Sluggable
  extend FriendlyId

  friendly_id :name, use: :slugged

  has_many :recipes

  validates :name, uniqueness: true

  mount_uploader :cover, KitchenCoverUploader

  default_scope -> { order(:name) }
end

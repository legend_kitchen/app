# frozen_string_literal: true

module Sluggable
  def to_param
    id
  end

  private

  def sluggable
    Russian.translit(name)
  end

  def should_generate_new_friendly_id?
    name_changed?
  end
end

# frozen_string_literal: true

class Product < ApplicationRecord
  has_many :ingredients
  has_many :recipes, through: :ingredients

  validates :name, uniqueness: true

  default_scope -> { order(:name) }
end

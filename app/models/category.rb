# frozen_string_literal: true

class Category < ApplicationRecord
  include Sluggable
  extend FriendlyId

  friendly_id :name, use: :slugged

  has_many :recipes

  validates :name, uniqueness: true

  mount_uploader :cover, CategoryCoverUploader

  default_scope -> { order(:name) }
end

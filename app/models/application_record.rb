# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.collection_for_select(field_name_to_show)
    all.map { |record| [record.send(field_name_to_show), record.id] }
  end
end

# frozen_string_literal: true

class Recipe < ApplicationRecord
  include Sluggable
  extend FriendlyId

  friendly_id :sluggable, use: :slugged

  belongs_to :category
  belongs_to :kitchen, optional: true

  has_many :ingredients
  has_many :products, through: :ingredients

  enum status: %i[fresh published archived]

  validates :name, presence: true, uniqueness: true
  validates :body, presence: true
  validates :status, presence: true

  mount_uploader :cover, RecipeCoverUploader

  accepts_nested_attributes_for :ingredients, allow_destroy: true
end

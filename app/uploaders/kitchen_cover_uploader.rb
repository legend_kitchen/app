# frozen_string_literal: true

class KitchenCoverUploader < BaseImageUploader
  def store_dir
    "uploads/kitchens/#{model.id}"
  end

  process resize_to_limit: [1200, 1200]

  def default_url(*_args)
    'default_kitchen_cover.png'
  end

  version :card do
    process resize_to_fit: [350, 350]
  end
end

# frozen_string_literal: true

class RecipeCoverUploader < BaseImageUploader
  def store_dir
    "uploads/recipes/#{model.id}"
  end

  process resize_to_limit: [1200, 1200]

  def default_url(*_args)
    'default_recipe_cover.png'
  end

  version :big do
    process resize_to_fit: [400, 300]
  end
  version :card do
    process resize_to_fit: [350, 260]
  end
end

# frozen_string_literal: true

class RecipeBodyImageUploader < BaseImageUploader
  def store_dir
    'uploads/recipes/images'
  end

  process resize_to_limit: [900, 900]
end

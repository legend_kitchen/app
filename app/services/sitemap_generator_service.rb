# frozen_string_literal: true

require 'rubygems'
require 'sitemap_generator'

class SitemapGeneratorService < ApplicationService
  include Rails.application.routes.url_helpers

  Contract Any
  def call
    generate_sitemap
    ping_search_engines if Rails.env.production?
  end

  private

  def generate_sitemap # rubocop:disable Metrics/MethodLength
    SitemapGenerator::Sitemap.default_host = root_url
    SitemapGenerator::Sitemap.create do
      add(kitchens_path, changefreq: 'weekly', priority: 0.8)
      Kitchen.find_each do |kitchen|
        add(kitchen_path(kitchen.slug), changefreq: 'weekly', priority: 0.8)
      end
      add(categories_path, changefreq: 'weekly', priority: 0.8)
      Category.find_each do |category|
        add(category_path(category), changefreq: 'weekly', priority: 0.8)
      end
      Recipe.published.find_each do |recipe|
        add(recipe_path(recipe), changefreq: 'monthly', priority: 0.8)
      end
    end
  end

  def ping_search_engines
    SitemapGenerator::Sitemap.ping_search_engines
  end
end

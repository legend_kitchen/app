# frozen_string_literal: true

require 'contracts'

class ApplicationService
  include Contracts::Core
  include Contracts::Builtin

  def self.call(*args, &block)
    new(*args, &block).call
  end
end

seed_file = Rails.root.join('db', 'seeds', 'development', 'kitchens.seeds.yml')
data = YAML.load_file(seed_file)

p('kitchens...')
data.each do |item|
  record = Kitchen.find_or_initialize_by(id: item.delete('id'))
  record.assign_attributes(item)
  record.save!
end

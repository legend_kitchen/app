seed_file = Rails.root.join('db', 'seeds', 'development', 'products.seeds.yml')
data = YAML.load_file(seed_file)

p('products...')
data.each do |item|
  record = Product.find_or_initialize_by(id: item.delete('id'))
  record.assign_attributes(item)
  record.save!
end

seed_file = Rails.root.join('db', 'seeds', 'development', 'categories.seeds.yml')
data = YAML.load_file(seed_file)

p('categories...')
data.each do |item|
  record = Category.find_or_initialize_by(id: item.delete('id'))
  record.assign_attributes(item)
  record.save!
end

body_file_path = Rails.root.join('db', 'seeds', 'development', 'recipes.seeds.body.html')
body_file = File.open(body_file_path, 'r')
body = body_file.read
body_file.close

p('recipes...')
50.times do |time|
  record = Recipe.find_or_initialize_by(id: time + 1)
  record.status = case time % 5
                  when 0
                    :fresh
                  when 1
                    :archived
                  else
                    :published
                  end
  record.name = "Hello! It's a test recipe ##{record.id}"
  record.category = Category.all.sample
  record.kitchen = Kitchen.all.sample unless time % 3 == 0
  record.body = body
  record.save!
  unless time % 10 == 0
    amounts = ['50 грамм', '200 грамм', '300 грамм']
    3.times do
      begin
        record.ingredients << Ingredient.create(product: Product.all.sample, amount: amounts.sample)
      rescue ActiveRecord::RecordNotUnique
        # Ignore
      end
    end
  end
end

class AddCategoryToRecipes < ActiveRecord::Migration[6.0]
  def change
    add_reference :recipes, :category
  end
end

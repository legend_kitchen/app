class AddSlugs < ActiveRecord::Migration[6.0]
  def change
    add_column :recipes, :slug, :string, null: false
    add_index :recipes, :slug, unique: true

    add_column :categories, :slug, :string, null: false
    add_index :categories, :slug, unique: true

    add_column :kitchens, :slug, :string, null: false
    add_index :kitchens, :slug, unique: true
  end
end

class CreateIngredients < ActiveRecord::Migration[6.0]
  def change
    create_table :ingredients do |t|
      t.references :product, foreign_key: true
      t.references :recipe, foreign_key: true

      t.string :amount

      t.timestamps

      t.index %i[product_id recipe_id], unique: true
    end
  end
end

class AddCoverToRecipes < ActiveRecord::Migration[6.0]
  def change
    add_column :recipes, :cover, :string
  end
end

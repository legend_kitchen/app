class CreateRecipes < ActiveRecord::Migration[6.0]
  def change
    create_table :recipes do |t|
      t.string :name, null: false, limit: 512
      t.text :body, null: false
      t.integer :status, null: false, default: 0

      t.timestamps

      t.index :name, unique: true
    end
  end
end

class AddCoverToCategoriesAndKitchens < ActiveRecord::Migration[6.0]
  def change
    add_column :categories, :cover, :string
    add_column :kitchens, :cover, :string
  end
end

class AddKitchenToRecipes < ActiveRecord::Migration[6.0]
  def change
    add_reference :recipes, :kitchen
  end
end

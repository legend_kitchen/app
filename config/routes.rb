# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  mount Sidekiq::Web => '/admin/sidekiq'

  root to: 'dashboard#index'

  resources :recipes, only: %i[show]

  resources :categories, only: %i[index show]

  resources :kitchens, only: %i[index show]
end
